#!/bin/bash

echo "show databases" | mongo da1.eecs.utk.edu | grep -i disc | awk '{print $1}'> mongodbs

cat mongodbs | while read i
do (echo "use $i"; echo "show collections") |   mongo da1.eecs.utk.edu
done | while read l
do if ( echo $l | grep switched >/dev/null ); then
       db=$(echo $l|awk '{print $4}');
       continue;
   fi
   if ( echo $l | grep '^bye' >/dev/null ); then db="";fi
   [[ $db != "" && $l != "system.indexes" ]] && echo $db";"$l
done > clc 

IFS=\;
cat clc | while read d c
do n=$((echo "use $d";echo "db.$c.count()") | mongo da1.eecs.utk.edu |grep ^[0-9])
   echo "$d;$c;$n"
done

