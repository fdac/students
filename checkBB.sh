#!/bin/bash


echo repositories/fdac | python3.5 bbC.py > /tmp/ou
perl -ane 's/, /,\n/g;print' < /tmp/ou |grep full_name | 
  perl -ane 's/.*: "//;s/",//;print;' | sort -u | grep -i fdac/d[0-9] > DiscRepos

(echo "use fdac16"; echo "db.d_commits.drop()";echo "db.d_issues.drop()") | mongo da1.eecs.utk.edu

for a in issues commits
do cat DiscRepos | python3.5 bbReposM.py d $a
done

python3 <<EOF | perl -ane 's/,/\n/g;print'
import pymongo
client = pymongo.MongoClient (host="da1.eecs.utk.edu")
db = client ['fdac16']
commits = db ['d_commits']
cnt = {}
for c in commits.find():
 repo = c['repository']['full_name']
 aur = c['author']['raw']
 auth = ''
 if 'user' in c ['author']: auth = c ['author']['user']['display_name']
 msg = c ['message']
 if not aur.startswith('Audris Mockus <audris@utk.edu>'):
  if aur in cnt:
   cnt[aur] += 1
  else:
   cnt[aur] = 1
print (cnt)
EOF
