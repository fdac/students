import os, sys, re, pymongo, json, time
import datetime
from requests.auth import HTTPBasicAuth
import requests


login = os.environ['BBID']
passwd = os.environ['BBPASS']

client = pymongo.MongoClient (host="da1.eecs.utk.edu")
db = client ['fdac16']

baseurl = 'https://api.bitbucket.org/2.0/repositories'
#do for followers following starred subscriptions orgs gists repos events received_events 
collName = 'pullrequests'
if (len (sys .argv) < 2):
  print ("Usage: " + sys .argv [0] + " assignment collection")
  sys.exit()

collName = sys .argv [2]

# Reference a particular collection in the database
collName1 = sys .argv [1] + '_' + re.sub('/','_',collName)
coll = db [collName1]

if collName == 'issues':
	collName = collName + '?state=all'

if collName == 'pullrequests':
	collName = collName + '?state=all'

def get (url):
  values = []
  size = 0
  try: 
    r = requests .get (url, auth=(login, passwd))
    time .sleep (0.1)
    n = 1
    print (url + " " + str(n))
    if (r.ok):
      t = r.text
      jt = json.loads (t)
      for el in jt['values']: values .append (el)
      while 'next' in  jt:
        url = jt['next']
        try: 
          r = requests .get(url, auth=(login, passwd))
          n += 1
          print (url + " " + str(n))
          if (r.ok): 
            t = r.text
            jt = json.loads (t)
            for el in jt['values']: values .append (el)
            size += len (t)
        except requests.exceptions.ConnectionError:
          sys.stderr.write('could not get ' + links + ' for '+ url + '\n')   
          #print u';'.join((u, repo, t)).encode('utf-8') 
      #print (url)
    else:
      print (url + ';ERROR')
  except requests.exceptions.ConnectionError:
    print (url + ';ERROR')
  return values, size

def chunks(l, n):
  if n < 1: n = 1
  return [l[i:i + n] for i in range(0, len(l), n)]

for n in sys .stdin:
  n = n .rstrip ()
  url = baseurl + '/' + n + '/' + collName
  url1 = url
  v = []
  size = 0
  try: 
    v, size = get (url1)
    #print (v)
    #print (str (len (v)) + ';' + str (size) + ';' + url1)
    sys .stdout .flush ()
  except Exception as e:
    sys.stderr.write ("Could not get:" + url1 + "\n")    
    print (e)
    break
  ts = datetime.datetime.utcnow()	
  if len (v) > 0:
    for val in v:
      coll.insert (val)
      #coll.insert ( { 'name': n, 'url': url, 'utc':ts, 'v' : val } )


